
# Prerequisites

Install your native toolchain and NativeScript as described in the docs:

https://docs.nativescript.org/setup/quick-setup


## Install dependencies

```
$ npm install -g grunt-cli gulp
```

Then install the needed NPM packages:

```
$ npm install
```

## Compile and prepare NativeScript and Angular

```
$ grunt
```

## Initialize the CSI app (csi-app)

```
$ cd csi-app
$ npm install
```

The latter installs the `angular2` and `tns-core-modules` packages that you just built by running `grunt prepare` step in the project root.

## Run the csi-app app

```
$ tns run android
$ tns run ios
```

#### Live/emulated development
```
$ build <Platform>
$ tns livesync <Platform> --watch
```