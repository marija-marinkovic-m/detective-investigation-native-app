import { Component, OnInit } from '@angular/core';
import * as barcodescanner from 'nativescript-barcodescanner';
import * as utility from 'utils/utils';

@Component({
    selector: 'home-screen', 
    template: `
    <ActionBar title="CSI:Training"></ActionBar>
    <StackLayout verticalAlignment="center">
        <Image src="~/assets/img/logo-new.png" width="164" height="115"></Image>
        <Button text="Scan evidence" (tap)="startScan($event)"></Button>
        
        <Label [text]="scanResult" [ngStyle]="{'background-color': 'gray'}"></Label>
    </StackLayout>
    `
})
export class HomeScreenComponent implements OnInit {
    public scanResult:string = '';
    constructor() {}
    
    ngOnInit() {}
    
    requestPermission() {
        return new Promise((resolve, reject) => {
            barcodescanner.available().then((available) => {
                if (available) {
                    barcodescanner.hasCameraPermission().then((granted) => {
                        if (!granted) {
                            barcodescanner.requestCameraPermission().then(() => {
                                resolve("Camera permission granted");
                            });
                        } else {
                            resolve("Camera permission was already granted.");
                        }
                    }); 
                } else {
                    reject("This device does not have an available camera");
                }
            });   
        });
    }
    
    startScan(event:any) {
        this.requestPermission().then((result) => {
            barcodescanner.scan({
                cancelLabel: "Stop scanning",
                message: "Place a evidence inside the viewfinder rectangle to scan it", 
                preferFrontCamera: false, 
                showFlipCameraButton: true, 
                orientation: "portrait"
            })
            .then((result) => {
               // *** do something with scan result :)
               // result = {format: '', text: ''}
               this.scanResult = result.text;
               utility.openUrl(result.text);
            }, (error) => {
                console.log('No scan: ' + error);
            })
        }, (error) => {
            console.log('ERROR', error);
        });
    }
}